from numpy import linalg as LA
import numpy as np

print('Siapkan ruang untuk matriks A')
i=int(input('Masukkan jumlah baris='))
j=int(input('Masukkan jumlah kolom='))

print(' ')
A = np.zeros((i,j))

print('## INPUT ELEMEN MATRIKS A ##')
for x in range(0,i):
    for y in range(0,j):
        print('A[',x,'][',y,']= ',end='')
        A[x][y]=input('')

print('Siapkan ruang untuk matriks B')
i=int(input('Masukkan jumlah baris='))
j=int(input('Masukkan jumlah kolom='))

print(' ')
B = np.zeros((i,j))

print('## INPUT ELEMEN MATRIKS B ##')
for x in range(0,i):
    for y in range(0,j):
        print('B[',x,'][',y,']= ',end='')
        B[x][y]=input('')

w, v = LA.eig(A)
print("MATRIX A:")
print(A)
print("\n\nMATRIX B:")
print(B)
print("\n\nNilai Eigen:")
print(v)
print("\n\nVektor Eigen:")
print(w)
